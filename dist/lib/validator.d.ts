import { OpenApiDocument } from 'express-openapi-validate';
import { RequestHandler } from 'express';
import { Operation } from 'express-openapi-validate/dist/OpenApiDocument';
export declare type Validator = (method: Operation, path: string) => RequestHandler;
export declare function getSchema(path: string): OpenApiDocument;
export declare function getValidator(path: string): Validator;
