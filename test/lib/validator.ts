import { describe, it } from 'mocha';
import fs from 'fs';
import sinon from 'sinon';
import chai from 'chai';
import sinonChai from 'sinon-chai';
import { getSchema } from '../../lib/validator';

chai.use(sinonChai);

const { expect } = chai;

describe('validator', () => {
	describe('getSchema', () => {
		it('reads the given file', () => {
			const path = 'some path';
			sinon.stub(fs, 'readFileSync').returns('');
			getSchema(path);
			expect(fs.readFileSync).to.have.been.calledOnceWith(path);
		});
	});
});
