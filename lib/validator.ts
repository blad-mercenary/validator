import { OpenApiValidator, OpenApiDocument } from 'express-openapi-validate';
import { readFileSync } from 'fs';
import { RequestHandler } from 'express';
import * as YAML from 'yaml';
import { Operation } from 'express-openapi-validate/dist/OpenApiDocument';

export type Validator = (method: Operation, path: string) => RequestHandler;

export function getSchema(path: string): OpenApiDocument {
	return YAML.parse(
		readFileSync(path, 'utf-8')
	);
}

export function getValidator(path: string): Validator {
	const validator = new OpenApiValidator(getSchema(path));
	return validator.validate.bind(validator);
}
